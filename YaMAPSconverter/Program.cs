﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;

namespace YaMAPSconverter
{
    class Program
    {
        
        static void Main(string[] args)
        {
            string Path = @"yamapng";
            string PathOuntput = @"yMaps";

            if (Directory.Exists(PathOuntput))
            {
                Console.WriteLine("Удалите папку {0}", PathOuntput);
                Console.ReadKey();
                return;
            }
            Directory.CreateDirectory(PathOuntput);

            if (!Directory.Exists(Path))
            {
                Console.WriteLine("Нет исходной директории с картинками {0}", Path);
                Console.ReadKey();
                return;
            }

            string[] Files = Directory.GetFiles(Path,"*.*",SearchOption.AllDirectories);
            int All = Files.Length;
            int Current = 0;
            int offsetZcoord = -1;
            
            Console.WriteLine("Обработка файлов карты\n");
            foreach (string CurrentPath in Files)
            {
                Current++;
                string[] Arr = CurrentPath.Split('\\');
                int count = Arr.Length - 1;
                string z = Arr[count - 4];
                z = "z"+(Convert.ToInt32(z.Substring(1)) + offsetZcoord).ToString();
                File.Copy(CurrentPath, string.Format("{0}\\{1}-{2}-{3}", PathOuntput, z, Arr[count - 2], Arr[count]));
             //   c
                drawTextProgressBar(Current, All);
            }

            int countInBase = System.IO.File.ReadAllLines("autodelete.txt").Length;

            FileStream Fs = new FileStream("autodelete.txt", FileMode.Open);
            StreamReader SR = new StreamReader(Fs);

            Console.WriteLine("\n\nУдаляем лишнее\n");

            
            int CurrentDelete = 0;
            while (!SR.EndOfStream)
            {

                string Line = SR.ReadLine();
                if (File.Exists(PathOuntput +"\\"+ Line)) File.Delete(PathOuntput +"\\"+ Line);
                drawTextProgressBar(++CurrentDelete, countInBase);
            }

            SR.Close();
            Fs.Close();


            Console.Write("\n\nГотово, обработано {0} файлов",All);
            Console.ReadKey();

        }

        private static void drawTextProgressBar(int progress, int total)
        {
            //draw empty progress bar
            Console.CursorLeft = 0;
            Console.Write("["); //start
            Console.CursorLeft = 32;
            Console.Write("]"); //end
            Console.CursorLeft = 1;
            float onechunk = 30.0f / total;

            //draw filled part
            int position = 1;
            for (int i = -1; i < onechunk * progress; i++)
            {
                Console.BackgroundColor = ConsoleColor.DarkGreen;
                Console.CursorLeft = position++;
                Console.Write(" ");
            }

            //draw unfilled part
            for (int i = position; i <= 31; i++)
            {
                Console.BackgroundColor = ConsoleColor.Gray;
                Console.CursorLeft = position++;
                Console.Write(" ");
            }

            //draw totals
            Console.CursorLeft = 35;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write(progress.ToString() + " of " + total.ToString() + "    "); //blanks at the end remove any excess
        }
    }
}
